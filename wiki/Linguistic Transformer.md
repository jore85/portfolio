# Linguistic Transformer #

** Dauer **  
Juni 2014 - Mai 2015

** Zugehörige Veröffentlichung **  
* Revising the Vocabulary of Business Process Element Labels *  
A. Koschmider, M. Ullrich, A. Heine, A. Oberweis  
2015  
<http://dx.doi.org/10.1007/978-3-319-19069-3_5>

** Eingesetzte Technologien/Libaries **

* [JDOM](http://www.jdom.org/) für das Parsen/Schreiben von [PNML](http://www.pnml.org/)-Dateien
* [Stanford PoS-Tagger](http://nlp.stanford.edu/software/tagger.shtml) zur Bestimmung von Wortarten
* [JWKTL](https://dkpro.github.io/dkpro-jwktl/) um Synonyme aus der Wiktionary zu finden
* [Leipziger Wortschatz Webservices](http://wortschatz.uni-leipzig.de/Webservices/) zur Abfrage von Daten aus dem Leipziger Wortschatz, einer Korpora-Sammlung