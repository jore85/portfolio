# Portfolio #
Die nachfolgenden Auflistungen geben einen Überblick über alle Projekte im Rahmen meines Informationswirtschaftsstudiums am Karlsruher Institut für Technologie und HiWi-Tätigkeiten in der Zeit von 2009 bis 2017.

## Projekte im Rahmen des Studiums ##

* Enigma Verschlüsselung - Programmieren, 2010
* Vier Gewinnt Client/Server-Version - Praxis der Telematik, 2012/2013 †
* Process Model Matcher - Bachelorarbeit, 2013/2014
* Debiasing Macroeconomic Forecasts - Seminar, 2015/2016 †

## Projekte im Rahmen einer HiWi-Tätigkeit ##

* Linguistic Transformer - Hiwi-Tätigkeit, 2014/2015 - Weitere Details zu [[Linguistic Transformer]]
* Betreuung von <a href="http://www.abida.de/">abida.de</a> (Drupal 7)

## Ausarbeitungen ##

|Titel | Art | Jahr |
|--- | --- | --- |
|SODEE: A System For Online Detection Of Emerging Entities|Master-Thesis|2016/2017|
|Kurzfristige Prädiktion von Wertpapierkursen auf Grundlage von Nachrichtenartikel durch Maschinelle Lernverfahren|Programmierpraktikum †|2015/2016|
|Debiasing Macroeconomic Forecasts|Seminar †|2015/2016|
|Gamification im Process Model Matching|Seminar|2014|
|Process Model Matching|Bachelor-Thesis|2013/2014|
|Petri Net Markup Language (PNML)|Seminar|2013|
|Wie beeinflussen Verlinkungen von Facebook-Freunden soziale Bindungen und wie ist dies datenschutzrechtlich zu bewerten?|Seminar †|2012

†: In Zusammenarbeit mit mehreren Studenten:innen.
