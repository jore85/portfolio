# Process Model Matcher #

**Dauer**  
Juni 2014 - Mai 2015

** Zugehörige Veröffentlichung **  
* Revising the Vocabulary of Business Process Element Labels *  
A. Koschmider, M. Ullrich, A. Heine, A. Oberweis  
2015  
<http://dx.doi.org/10.1007/978-3-319-19069-3_5>