# Willkommen auf meinem Code-Portfolio #

Hier finden Sie eine Auswahl über verschiedene Projekte, die ich im Laufe der Zeit bearbeitet habe.

## Projekte im Rahmen des Studiums ##

* Enigma Verschlüsselung - Programmieren, 2010
* Vier Gewinnt Client/Server-Version - Praxis der Telematik, 2012/2013
* Process Model Matcher - Bachelorarbeit, 2013/2014

## Projekt im Rahmen einer Hiwi-Tätigkeit ##

* Linguistic Transformer - Hiwi-Tätigkeit, 2014/2015 - [Details](https://bitbucket.org/jore85/portfolio/src/LinguisticTransformer/?at=master "Details")

## Ausarbeitungen ##

* Gamification im Process Model Matching - Seminar, 2014
* Process Model Matching - Bachelorarbeit, 2013/2014
* Petri Net Markup Language (PNML) - Seminar, 2013
* Verlinkungen auf Facebook - Seminar, 2012